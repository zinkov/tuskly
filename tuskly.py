# Install deps: pip3 install streamlit Mastodon.py
from datetime import datetime

import streamlit as st
import streamlit.components.v1 as components
from mastodon import Mastodon


class Style:
    def __init__(self, f):
        self.f = f

    def _repr_html_(self):
        return "<style>" + open(self.f).read() + "</style>"


def post_message(msg, visibility, file_data=None, file_desc=None, **kwargs):
    media_ids = None
    if file_data is not None:
        mime_type = file_data.type
        media_dict = mastodon.media_post(
            file_data.read(), mime_type=mime_type, description=file_desc
        )
        media_ids = [media_dict["id"]]
    mastodon.status_post(msg, visibility=visibility, media_ids=media_ids, **kwargs)


st.title("Mastodon posting dashboard")

server = st.text_input("Server:", value="https://babka.social", key="server_name")
token = st.text_input("Mastodon Access Token", key="access_token")

if st.session_state.access_token == "":
    st.stop()

mastodon = Mastodon(
    api_base_url=st.session_state.server_name,
    access_token=st.session_state.access_token,
)

uid = mastodon.me().id
posts = mastodon.account_statuses(uid)
num_posts = 5

st.write(Style("css/social.css"), unsafe_allow_html=True)
st.markdown(f"## Last {num_posts} posts")
for p in posts[:num_posts]:
    if p.reblog is not None:
        p = p["reblog"] 
    h = f"""
            <blockquote class="toot-blockquote" cite="{p.account.url}">
                <div class="toot-header">
                    <a class="toot-profile" href="{p.account.url}" rel="noopener">
                        <img
                            src="{p.account.avatar}"
                            alt="Mastodon avatar for {p.account.acct}"
                            loading="lazy"
                        />
                    </a>
                    <div class="toot-author">
                        <a class="toot-author-name" href="{p.account.url}" rel="noopener">{p.account.display_name}</a>
                        <a class="toot-author-handle" href="{p.account.url}" rel="noopener">{p.account.acct}</a>
                    </div>
                </div>
                {p.content}
    <div class="toot-footer">
       <a href="{p.uri}" class="toot-date" rel="noopener">{p.created_at.strftime("%d %b %Y, %H:%M (%Z)")}</a>
    </div>
    </blockquote>
    """
    st.write(h, unsafe_allow_html=True)
    # st.write("""<div style="border:1px solid black;">""", unsafe_allow_html=True)
    # st.write(p.content, unsafe_allow_html=True)
    # st.write("</div>", unsafe_allow_html=True)


media_upload = st.checkbox("Media Upload?")
file_data = None
file_desc = None
if media_upload:
    file_desc = st.text_input("File description: (Mandatory)")
    file_data = st.file_uploader("File to upload:", disabled=file_desc == "")
    if file_data is not None:
        st.image(image=file_data, caption=file_desc)

later = st.checkbox("Schedule Post?")

with st.form(key="post_form"):
    st.markdown("## Post a message")
    msg = st.text_area(
        "Message:",
        placeholder="What do you have in mind?",
    )
    reply_url = st.text_input(
        "In Reply To URL:",
        placeholder="Optional: Reply URL",
    )
    if reply_url == "":
        in_reply_to_id = None
    else:
        search_results = mastodon.search(reply_url).statuses
        if len(search_results) > 0:
            in_reply_to_id = search_results[0].id
        else:
            in_reply_to_id = None

    visibility = st.radio(
        "Posting Visibility:", ("public", "unlisted", "private", "direct")
    )
    if later:
        now = datetime.now()
        d = st.date_input("Day", now.date(), min_value=now.date())
        t = st.time_input("Time", now.time())
        scheduled_at = datetime.combine(d, t)
    else:
        scheduled_at = None

    submit = st.form_submit_button(label="Post")

if submit:
    post_message(
        msg,
        visibility,
        in_reply_to_id=in_reply_to_id,
        scheduled_at=scheduled_at,
        file_data=file_data,
        file_desc=file_desc,
    )
