# Tuskly - Simple dashboarding for managing shared Mastodon accounts

This is a [streamlit](https://streamlit.io/) dashboard for managing a Mastodon account.

Run it using the following command

````bash
streamlit run tuskly.py
````
